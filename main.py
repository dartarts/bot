"""
Implementation of Bot for blog application.
Currently bot support folowing actions:
- signup
- login
- create post
- like/dislike post
"""

import settings
import asyncio
import logging
import settings
import json
from random import randint, sample
from models import User
from utils import get_random_string

class Bot:

    def __init__(self, name, number_of_users=0, max_posts_per_user=0, max_likes_per_user=0):
        self.name = name
        self.number_of_users = number_of_users
        self.max_posts_per_user = max_posts_per_user
        self.max_likes_per_user = max_likes_per_user
        self.users = dict() # map: username => user data
        self.posts = dict() # map: url => post

    async def create_users(self):
        """Generate new users on REST."""
        while len(self.users) < self.number_of_users:
            user = User()
            data = await user.api.signup(dict(username=user.username, password=user.password, email=user.email, name=user.name))
            if data:
                user.data = data
                self.users[user.username] = user
                logging.warning('Create user: %s => %s' % (user.username, user.data.get('url')))

    async def login_users(self):
        """Login users to REST."""
        while True:
            tasks = [asyncio.ensure_future(user.api.login(user.username, user.password)) for username, user in dict(self.users).items() if not user.api.token]
            if tasks:
                await asyncio.gather(*tasks, return_exceptions=True)
                logging.warning('Login %s users' % len(tasks))
            await asyncio.sleep(settings.TASK_TIMEOUT)

    async def create_random_posts(self):
        """Create random posts for user."""
        while True:
            tasks = list()
            for username, user in self.users.items():
                if not user.api.token:
                    continue
                if self.max_posts_per_user > len(user.posts):
                    random_number_of_posts = randint(1, (self.max_posts_per_user - len(user.posts))) # Get random number of posts to generate by user
                    # Generate posts to create on REST
                    for _ in range(random_number_of_posts):
                        post = await user.api.create_post(dict(title=get_random_string(10), text=get_random_string(100), author=user.data.get('url')))
                        if post:
                            user = self.users[post['username']]
                            user.posts[post['url']] = post
                            self.posts[post['url']] = post
                    logging.warning('User %s has %s posts.' % (username, len(user.posts)))
                else:
                    logging.warning('Cant create new post for %s, because user already has %s posts, while max number per user: %s' % (username, len(user.posts), self.max_posts_per_user))
            await asyncio.sleep(settings.TASK_TIMEOUT)

    async def like_random_posts(self):
        """Like random posts."""
        while True:
            if self.posts:
                tasks = list()
                for username, user in self.users.items():
                    if not user.api.token:
                        continue
                    if self.max_likes_per_user > user.likes:
                        random_number_of_likes = randint(1, (self.max_likes_per_user - user.likes)) # Get random number of likes for user
                        if random_number_of_likes > len(self.posts):
                            random_number_of_likes = len(self.posts)
                        random_posts = sample(list(self.posts.keys()), random_number_of_likes) # Get random posts to like
                        for post_url in random_posts:
                            post = self.posts[post_url]
                            post = await user.api.like_the_post(post['url'], post)
                            if post:
                                self.users[username].likes += 1
                                self.users[post['username']].posts[post['url']] = post
                                self.posts[post['url']] = post
                                logging.warning('User %s has %s likes. Post has %s likes.' % (post['username'], self.users[username].likes, post['like']))
                    else:
                        logging.warning('Cant like the post, because user %s has max number of likes: %s' % (username, user.likes))
            await asyncio.sleep(settings.TASK_TIMEOUT)

    async def refresh_tokens(self):
        """Refresh jwt token."""
        while True:
            tasks = [asyncio.ensure_future(user.api.refresh_token()) for username, user in dict(self.users).items()]
            await asyncio.gather(*tasks, return_exceptions=True)
            logging.warning('Refresh %s tokens completed!' % len(tasks))
            await asyncio.sleep((60 * 5)) # every 5 minutes

    async def close_sessions(self):
        """Close all sessions."""
        for username, user in self.users.items():
            if user.api.token:
                await user.api.close()

if __name__ == '__main__':
    with open(settings.CONFIG_FILE, 'r') as f:
        config = json.load(f)
    bot = Bot('Test', number_of_users=config['NUMBER_OF_USERS'], max_posts_per_user=config['MAX_POSTS_PER_USER'], max_likes_per_user=config['MAX_LIKES_PER_USER'])
    loop = asyncio.get_event_loop()
    loop.run_until_complete(asyncio.gather(*[
        bot.create_users(),
        bot.login_users(),
        bot.create_random_posts(),
        bot.like_random_posts(),
        # bot.refresh_tokens(),
    ]))




    
    
    