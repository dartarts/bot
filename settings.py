"""
Module for bot settings.
"""

EMAIL_DOMAIN = '@instagram.com'
CONFIG_FILE = 'config.json'
TASK_TIMEOUT = 10 # sec