"""
Module which provide REST API.
"""

import aiohttp
from urls import routes

class Rest:

    def __init__(self):
        self.session = aiohttp.ClientSession()
        self._token = dict()

    @property
    def token(self):
        return self._token

    def get_headers(self):
        """Return jwt headers."""
        headers = {"Authorization": "Bearer %s" % self._token.get('access')}
        return headers

    async def get_token(self, username, password):
        """Request new jwt token."""
        response = await self.session.post(url=routes.get('token'), data={'username' : username, 'password' : password})
        if response.status == 200:
            return await response.json()
        return dict()

    async def refresh_token(self):
        """Refresh jwt token."""
        data = dict(refresh=self._token.get('refresh'))
        response = await self.session.post(url=routes.get('refresh-token'), data=data)
        if response.status == 200:
            self._token = await response.json()
    
    async def login(self, username, password):
        """Login to API, after setup jwt tokens."""
        self._token = await self.get_token(username, password) # setup jwt tokens

    async def signup(self, user_data):
        """Create new user."""
        response = await self.session.post(url=routes.get('signup'), data=user_data)
        if response.status == 201:
            return await response.json()

    async def create_post(self, data):
        """Create new post."""
        headers = self.get_headers()
        response = await self.session.post(url=routes.get('create-post'), headers=headers, data=data)
        if response.status == 201:
            return await response.json()

    async def update_post(self, url, data):
        """Update post."""
        headers = self.get_headers()
        response = await self.session.put(url, headers=headers, data=data)
        if response.status == 200:
            return await response.json()

    async def like_the_post(self, url, data):
        """Like the post."""
        data['like'] += 1
        return await self.update_post(url, data)

    async def close(self):
        """Close the session."""
        await self.session.close()

