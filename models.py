import rest
import settings
from utils import get_random_string


class User:
    def __init__(self):
        self.username = get_random_string(5)
        self.password = get_random_string(7)
        self.name = get_random_string(5) + ' ' + get_random_string(4)
        self.api = rest.Rest()
        self.posts = dict() # dict of posts for current user, map: url => post
        self.data = dict() # response from REST, basicaly json of user data
        self.likes = 0
    
    @property
    def email(self):
        return self.username + settings.EMAIL_DOMAIN
