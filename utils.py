"""Helper functions"""

import random
import string

def get_random_string(length):
    _str =  ''.join([random.choice(string.ascii_letters + string.digits) for n in range(length)])
    return _str

