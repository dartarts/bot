"""
Define routes for REST API.
"""

routes = {
    'signup' : 'http://127.0.0.1:8000/api/users/',
    'token' : 'http://127.0.0.1:8000/api/token/',
    'refresh-token' : 'http://127.0.0.1:8000/api/token/refresh/',
    'create-post' : 'http://127.0.0.1:8000/api/posts/',
}
